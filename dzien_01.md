# Dzień pierwszy – instalacja środowiska

## Instalacja zintegrowanego środowiska programistycznego (IDE)

Do wyboru jedno z:
1. [WebStorm](https://www.jetbrains.com/webstorm/download/) (bardzo łatwy w obsłudze/**płatny**, ale jest trial)
2. [Visual Studio Code](https://code.visualstudio.com/Download) (łatwy w obsłudze/bezpłatny)
3. [Atom](https://atom.io/) (umiarkowanie trudny w obsłudze/bezpłatny)
4. [Vim](https://github.com/vim/vim-win32-installer/releases) (bardzo trudny w obsłudze/bezpłatny)

## Systemu kontroli wersji GIT

Git służy do edycji projektów informatycznych w środowisku rozproszonym.
Czyli do wymiany kodu pomiędzy programistami. 
Dobry i dokładny tutorial znajduje się na [tej stronie](https://git-scm.com/book/pl/v2) oraz w darmowym [wideo-kursie](https://www.youtube.com/watch?v=D6EI7EbEN4Q&list=PLjHmWifVUNMKIGHmaGPVqSD-L6i1Zw-MH).

### Instalacja

Program można ściągnąć ze [stony oficjalnej](https://git-scm.com/downloads).

### Konfiguracja konta

Do wyboru są 2 platformy:
1. [gitlab](https://about.gitlab.com/) (można zalogować się za pomocą konta github)
2. [github](https://github.com/)

Należy założyć konto na mimimum jednym z nich.

### Zadanie do samodzielnego wykonania
Zadanie znajduje się [tutaj](tasks/zad_git.md). 

## Node.js

Jest to środowisko do uruchamiania skryptów w języku javascript.
Należy je [pobrać](https://nodejs.org/en/download/) i zainstalować.
Dobrze używać wersji LTS.

## Docker

Aby ograniczyć zależności pomiędzy wersjami środowisk będziemy korzystać z *dockera*. 
Opis programu znajduje się w [tym filmie](https://www.youtube.com/watch?v=gAkwW2tuIqE).

### Instalacja

Środowisko należy ściągnąć z [oficjalnej storny](https://hub.docker.com/editions/community/docker-ce-desktop-windows/).
Dobrze używać wersji STABILNEJ.

## Baza danych

Aby przechowywać dane na serwerze potrzebna jest baza danych. 
Będziemy korzystali z Postgres'a.

### Instalacja przeglądarki baz danych

Aby ułatwić sobie sposób korzystania z baz danych proponuję program [DBeaver](https://dbeaver.io/download/).

### Instalacja bazy danych poprzez dockera – zadanie do samodzielnego wykonania

Pomocne mogą się okazać poniższe komendy:

> docker pull postgres

> docker run -d --name tt_postgres -p 5432:5432 -e POSTGRES_PASSWORD=password -e PGDATA=/pgdata -v /pgdata:/pgdata postgres

Można zweryfikować poprzez komendę:

> docker ps

### Utworzenie bazy danych

Można wykonać za pomocą konsoli:
> psql -h localhost -p 5432 -U postgres
>
> CREATE DATABASE TTranslantions OWNER postgres ENCODING = 'UTF8';

Lub za pomocą programu do zarządzania bazami danych.

Należy zweryfikować poprzez DBeaver.

![alt text](img/postgres_1.png "Utworzona baza danych")

## W wolnym czasie
[Postgres in Docker](https://www.youtube.com/watch?v=iZDbENJrl4I)

[10 mitów o Unicode](https://www.youtube.com/watch?v=QIEpZ0MGoBc) – wiedza ogólna



