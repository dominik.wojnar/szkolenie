# Dzień czwarty – wykorzystanie narzędzi AI

## Instalacja PyCharm (opcjonalne)

Do projektu przyda się IDE, polecam darmową wersję [PyCharm](https://www.jetbrains.com/pycharm/download/) (Community Edition)

## Instalacja Whisper AI

Biblioteka [Whisper](https://openai.com/research/whisper) służy do robienia transkrypcji (speech-2-text)

Proces instalacji znajduje się na [tej](https://github.com/openai/whisper) stronie. 

## Test Whisper AI - stayczny plik

Po instalacji polecam sprawdzić poprawność działania. 

Wykorzystaj model `tiny.en` na pliku `examples/lecture.wav` wynik możesz porównać ze [źródłem](https://youtu.be/P9UQi0ORXv4?si=1g6uHPpr3RG-5js1&t=1419).

> whisper examples/lecture.wav --model tiny.en --language English

Wynik zapisałem jako `examples/lecture.txt`.

Jeśli chcesz powtórz to na większym modelu.

## Test Whisper AI - transkrypcja live

--- **TODO** ---
