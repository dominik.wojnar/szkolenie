# Najważniejsze komendy GIT – zadanie

Należy sklonować projekt, wypełnić poniższe komendy i wysłać te zmiany na serwer.
Na brancha zawierającego w nazwie Twoje imię i nazwisko.

> git ...

inicjalizuje projekt w danym katalogu

> git ...

utworzenie nowej gałęzi o nazwie *my-branch*

> git ...

przejście na gałąź o nazwie *my-branch*

> git ...

dodaje plik o nazwie *main.ts* do repozytorium


> git ...

pakuje zmiany w paczkę tzw. commit

> git ...

pokazuje stan projektu


> git ...

pobiera zmiany z serwera *origin*, z gałęzi *my-branch* i dołącza do listy aktualnych zmian

> git ...

wysyła wszystkie zmiany na serwer *origin* na gałąź *master*

Plik tekstowy **.gitignore** zawiera reguły (regexpr) jakie pliki będą ignorowane przy wysyłaniu na serwer.
Utwórz taki plik i dodaj regułę ignorowania:
1. zawartości katalogu *build*
2. pliku *src/tmp.js*
